import { config } from "dotenv"
config()
import express from "express"
const app = express()
import morgan from "morgan"
import router from "./src/routes"
import errorMiddleware from "./src/middleware/error.middleware"
import { ConnectMongoDB } from "./src/config/mongo/db"
import pgPackage from "./src/config/postgres/pgPackage"

app.use(morgan("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static("public"));
app.set("view engine", "ejs")


// * Routes
app.use(router)
app.use(errorMiddleware)
pgPackage

app.get("/home", (req, res) => {
    res.render("index")
})

app.listen(process.env.PORT, () => {
    console.log("running on port ", process.env.PORT);
    ConnectMongoDB()
})