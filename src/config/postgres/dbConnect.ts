import { Sequelize, Transaction } from "sequelize"
import dotenv from "dotenv"
// import transaction from "sequelize/types/transaction"
dotenv.config()

const dbName = "test"
const dbUserName = "postgres"
const dbPassword = "belajar"
const dbHost = "127.0.0.1"
const dialect = "postgres"

// const transaction = new transaction()

const sequelizeConnection = new Sequelize(dbName, dbUserName, dbPassword, {
    host: dbHost,
    dialect: dialect,
    password: dbPassword,
    pool: {
        max: 5,
        min: 0,
        idle: 10000,
        acquire: 30000,
    },
    define: {
        paranoid: true
    },
    query: {
        raw: true,
    }
})


// sequelizeConnection.transaction()

export default sequelizeConnection
