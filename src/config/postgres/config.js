module.exports = {
    development: {
        username: "postgres",
        password: "belajar",
        database: "test",
        host: "127.0.0.1",
        dialect: "postgres",
        timezone: "Asia/Jakarta",
        define: {
            timestamps: true,
            paranoid: true,
            underscored: true,
            underscoredAll: true,
            freezeTableName: true,
            createdAt: "created_at",
            updatedAt: "updated_at",
            deletedAt: "deleted_at",
            defaultScope: {
                attributes: {
                    exclude: [
                        "created_at",
                        "updated_at",
                        "deleted_at",
                        "created_by",
                        "updated_by",
                    ],
                },
            },
        },
        pool: {
            max: 5,
            min: 0,
            idle: 10000,
            acquire: 30000,
        },
        define: {
            paranoid: true,
        },
        query: {
            raw: true,
        },
    },
    test: {
        username: "root",
        password: null,
        database: "database_test",
        host: "127.0.0.1",
        dialect: "mysql",
    },
    production: {
        username: "root",
        password: null,
        database: "database_production",
        host: "127.0.0.1",
        dialect: "mysql",
    },
};
