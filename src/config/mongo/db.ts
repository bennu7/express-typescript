import { connect } from "mongoose"
import dotenv from "dotenv"
import { AppError } from "../../middleware/error.middleware"
dotenv.config()

export const ConnectMongoDB = async () => {
    connect(process.env.MONGO_URI as string, {
        maxPoolSize: 10,
        minPoolSize: 5,
    }).then(() => {
        console.log("Connected to MongoDB")
    }).catch((err) => {
        throw new AppError(500, "Error connecting to MongoDB")
    })
}