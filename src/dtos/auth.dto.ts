import {
    IsString,
    IsEmail,
    IsStrongPassword,
    MaxLength,
    IsNotEmpty,
    IsOptional
} from "class-validator"

export class RegisterUserDto {
    @IsEmail()
    @IsString()
    public email!: string;

    @IsString()
    @IsNotEmpty()
    public password!: string;

    @IsString()
    @IsNotEmpty()
    public full_name!: string;

    @IsString()
    @IsNotEmpty()
    public user_name!: string;

    @IsString()
    @IsNotEmpty()
    public gender!: string;
}

export class LoginUserDto {
    @IsEmail()
    @IsString()
    public email!: string;

    @IsString()
    @IsNotEmpty()
    public password!: string;
}