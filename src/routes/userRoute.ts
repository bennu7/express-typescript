import { Router } from 'express';
const router: Router = Router();
import { index, create } from '../controllers/userController';

router.post("/", create);


export default router;