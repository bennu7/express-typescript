import { Router } from 'express';
import { AppError } from '../middleware/error.middleware';
import { index, create } from '../controllers/todoController';

const router: Router = Router();


router.get("/", index);
router.post("/", create);

export default router;