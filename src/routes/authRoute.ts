import { Router } from 'express';

import AuthController from '../controllers/authController';
import ValidationMiddleware from '../middleware/validation.middleware';
import { LoginUserDto, RegisterUserDto } from '../dtos/auth.dto';


const router: Router = Router();
const authController = new AuthController()

router.post(
    "/auth/register",
    ValidationMiddleware(RegisterUserDto, "body"),
    authController.registerController
);

router.post(
    "/auth/login",
    ValidationMiddleware(LoginUserDto, "body"),
    authController.loginController
)

export default router;