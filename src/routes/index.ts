import { Router } from 'express';

const router: Router = Router();
import todoRoutes from './todoRoute';
import authRoutes from './authRoute';
import userRoutes from './userRoute';
import roleRoutes from './roleRoute';

router.use('/api/v1', todoRoutes);
router.use('/api/v1', authRoutes);
router.use('/api/v1', userRoutes);
router.use('/api/v1', roleRoutes);

export default router;
