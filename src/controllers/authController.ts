import { RegisterUserDto } from "@/dtos/auth.dto";
import AuthService from "../services/auth.services";
import { Request, Response } from "express";

class AuthController {
    public authSvc = new AuthService()

    public registerController = async (req: Request, res: Response) => {
        const userData: RegisterUserDto = req.body
        const userServiceResponse = await this.authSvc.registerSvc(userData)

        res.status(userServiceResponse.code).json(userServiceResponse)
    }

    public loginController = async (req: Request, res: Response) => {
        const userData: RegisterUserDto = req.body
        const userServiceResponse = await this.authSvc.loginSvc(userData)

        res.status(userServiceResponse.code).json(userServiceResponse)
    }

}

export default AuthController