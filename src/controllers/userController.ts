import { NextFunction, Request, Response } from "express"
import { users } from "../db/mongo/models/User"
import { AppError } from "../middleware/error.middleware"

export const index = async (req: Request, res: Response) => {
    try {
        const user = await users.find()

        // res.render("index", { user })
        res.status(200).json({ user })
    } catch (error) {
        console.log(error);
        throw new AppError(500, error as string)
    }
}

export const create = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { name, active, email, password } = req.body

        const checkUser = await users.findOne({ email })
        if (checkUser) {
            throw new AppError(400, "User already exists")
        }

        // const hashPassword = hashSync(password, 10)

        const user = await users.create({
            name,
            active,
            email,
            password
        })


        // const token = assignToken(user.id)


        // res.render("index", { user })
        res.status(201).json({ "message": "success created" })
    }
    catch (error: any) {
        next(error)
        // console.log("error => ", error);
        // if (error.code === 11000) {
        //     delete error.index
        //     delete error.code
        //     res.status(400).json(
        //         {
        //             message: "User already exists",
        //             error: error.keyValue,
        //         }
        //     )
        //     return
        // }
        // // throw new AppError(500, error as string)
        // return res.status(500).json({ error })
    }
} 