import { Request, Response } from "express"
import { users } from "../db/mongo/models/User"
import { AppError } from "../middleware/error.middleware"

export const index = async (req: Request, res: Response) => {
    try {
        const user = await users.find()

        // res.render("index", { user })
        res.status(200).json({ user })
    } catch (error) {
        console.log(error);
        throw new AppError(500, error as string)
    }
}

export const create = async (req: Request, res: Response) => {
    try {
        // throw new AppError(400, "app lauu")
        const user = await users.create({
            name: "lalu ibnu",
            active: true,
            email: "ibnu@gmail.com",
            password: "123456"
        })

        if (!user) {
            throw new AppError(400, "User already exists")
        }

        // res.render("index", { user })
        res.status(200).json({ user })
    }
    catch (error) {
        console.log(error);
        throw new AppError(500, error as string)
    }
} 