import jwt from "jsonwebtoken"
import { config } from "dotenv"
config()
const { JWT_SECRET } = process.env

export default class JWT {
    public static assignToken(payload: object): string {
        const dataPayload = jwt.sign(payload, JWT_SECRET as string, {
            expiresIn: "1h"
        })
        return dataPayload
    }

    public static verifyToken(token: string) {
        return jwt.verify(token, JWT_SECRET as string)
    }
}

// export const assignToken = (payload: any) => {
//     return jwt.sign(payload, JWT_SECRET as string, { expiresIn: "10m" })
// }

// export const verifyToken = (token: any) => {
//     return jwt.verify(token, JWT_SECRET as string)
// }