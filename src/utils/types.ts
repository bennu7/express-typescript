export interface IUser {
    name: string
    email: string
    password: string
    active: boolean
}

// *Location model
export interface ILocation {
    country: string
    city: string
}
export interface ICompany {
    name: string
    location: ILocation
    companySize: number
    email: string
    about: string
    website: string
    userID: any
}

// *Job model
type jobtTypes = "office" | "remote" | "hybrid"
type seniorityTypes = "entry" | "junior" | "intermediate" | "senior"

export interface ISalaryRange {
    min: number
    max: number
}

export interface IJob {
    title: string
    description: string
    seniority: seniorityTypes
    type: jobtTypes
    salaryRange: ISalaryRange
    link: string
    openingDate: Date
    closingDate: Date
    companyID: any
}
