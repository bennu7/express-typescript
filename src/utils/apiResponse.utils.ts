import { APIResponseInterface } from "@/interfaces/ApiResponse.interfaces";
import { StatusCodes as status } from "http-status-codes";


export function apiResponse(
    code: number,
    responseStatus: string,
    message: string,
    data?: any
): APIResponseInterface {
    return {
        code,
        status: responseStatus,
        message,
        data
    }
}