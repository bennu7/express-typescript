import User from "../db/postgres/models/User.model";
import UserRole from "../db/postgres/models/User_Role.model";
import { LoginUserDto, RegisterUserDto } from "../dtos/auth.dto";
import { APIResponseInterface } from "../interfaces/ApiResponse.interfaces";
import { UserInterface } from "../interfaces/User.interfaces";
import { apiResponse } from "../utils/apiResponse.utils";
import PasswordHasher from "../utils/passwordHash.utils";
import JWT from "../utils/jwt.utils";
import { StatusCodes as status } from "http-status-codes"
const GUEST_ID = "8cb07c50-0735-4df8-8e51-8f15c3fb3a5d";
import sequelizeConnection from "../config/postgres/dbConnect";

class AuthService {
    public registerSvc = async (userData: RegisterUserDto): Promise<APIResponseInterface> => {
        const t = await sequelizeConnection.transaction()
        try {
            const findEmail: UserInterface | null = await User.findOne({
                where: {
                    email: userData.email
                }
            })

            if (findEmail) {
                return apiResponse(status.BAD_REQUEST, "FAILED", "EMAIL HAS ALREADY")
            }

            const hashed = await PasswordHasher.hashPassword(userData.password)
            const createdUser = await User.create({
                email: userData.email,
                password: hashed,
                user_name: userData.user_name,
                full_name: userData.full_name,
                gender: userData.gender,
            }, {
                transaction: t,
                raw: true
            })

            await UserRole.create({
                user_id: createdUser.id,
                role_id: GUEST_ID
            }, {
                transaction: t
            })

            await t.commit()
            return apiResponse(201, "SUCCESS", "Success created a new account", createdUser)
        } catch (error: any) {
            await t.rollback()
            let message: any, statusResponse: any
            switch (error.name) {
                case "SequelizeForeignKeyConstraintError":
                    switch (error.parent.code) {
                        case "23503":
                            statusResponse = "FAILED"
                            message = error.original.detail
                            break
                    }
                    break;
                default:
                    statusResponse = "ERROR"
                    message = "ERROR"
            }
            return apiResponse(status.BAD_REQUEST, statusResponse, message)
        }
    }

    public loginSvc = async (userData: LoginUserDto): Promise<APIResponseInterface> => {
        try {
            const findEmail = await User.findOne({
                where: {
                    email: userData.email
                },
            })
            if (!findEmail) {
                return apiResponse(status.NOT_FOUND, "FAILED", "EMAIL NOT FOUND")
            }

            const comparePassword = await PasswordHasher.comparePassword(userData.password, findEmail.password)
            if (!comparePassword) {
                return apiResponse(status.UNAUTHORIZED, "FAILED", "PASSWORD IS WRONG")
            }
            const payload = {
                id: findEmail.id,
            }

            const token = JWT.assignToken(payload)

            return apiResponse(status.OK, "SUCCESS", "SUCCESS LOGIN", token)
        } catch (error) {
            console.log("error :>> ", error);
            console.log("error :>> ", JSON.parse(JSON.stringify(error)));

            return apiResponse(status.INTERNAL_SERVER_ERROR, "ERROR", "ERROR")
        }
    }
}

export default AuthService