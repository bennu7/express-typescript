"use strict";
const { v4 } = require("uuid");

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert(
            "roles",
            [
                {
                    id: v4(),
                    roleName: "guest",
                    active: false,
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        );
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete("roles", null, {});
    },
};
