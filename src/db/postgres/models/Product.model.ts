import { Sequelize, DataTypes, Model, Optional, UUIDV4 } from "sequelize"
import sequelizeConnection from "../../../config/postgres/dbConnect";
import { UUID } from "crypto"

interface IProductAttributes {
    id?: UUID
    role_name?: string | null
    active?: boolean | null
    created_at?: Date
    updated_at?: Date
}

class Product extends Model {
    public id!: UUID
    public role_name!: string
    public active!: boolean

    public readonly created_at!: Date
    public readonly updated_at!: Date
}

Product.init({
    id: {
        allowNull: false,
        primaryKey: true,
        type: UUIDV4,
        defaultValue: DataTypes.UUIDV4,
    },
    active: {
        allowNull: false,
        type: DataTypes.BOOLEAN,

    },
    role_name: {
        allowNull: false,
        type: DataTypes.STRING,
    }
}, {
    tableName: 'roles',
    timestamps: true,
    sequelize: sequelizeConnection,
    underscored: true,
})

export default Product