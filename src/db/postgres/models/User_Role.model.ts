import { Model, DataTypes, CreationOptional } from "sequelize";
import sequelizeConnection from "../../../config/postgres/dbConnect";
import Role from "./Role.model";
import User from "./User.model";

class UserRole extends Model {
    public id!: CreationOptional<string>;
    public user_id!: string;
    public role_id!: string;

    public role?: Role;
}

UserRole.init(
    {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
        },
        user_id: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: Role,
                key: "id",
            },
            onUpdate: "CASCADE",
            onDelete: "CASCADE",
        },
        role_id: {
            type: DataTypes.STRING,
            allowNull: false,
            references: {
                model: User,
                key: "id",
            },
            onUpdate: "CASCADE",
            onDelete: "CASCADE",
        },
        created_at: {
            type: DataTypes.TIME,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.TIME,
            defaultValue: DataTypes.NOW
        },
    },
    {
        tableName: "user_roles",
        sequelize: sequelizeConnection,
        timestamps: false,
        freezeTableName: true,
        underscored: true,
    },
);

// UserRole.belongsTo(Role, { foreignKey: "role_id", as: "role" })

export default UserRole;
