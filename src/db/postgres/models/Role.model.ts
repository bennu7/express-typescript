import { Sequelize, DataTypes, Model, Optional, UUIDV4 } from "sequelize"
import sequelizeConnection from "../../../config/postgres/dbConnect";
import { UUID } from "crypto"
import UserRole from "./User_Role.model";
import User from "./User.model";

interface IRoleAttributes {
  id?: UUID
  role_name?: string | null
  created_at?: Date
  updated_at?: Date
}


class Role extends Model {
  public id!: UUID
  public role_name!: string
  public active!: boolean

  public readonly created_at!: Date
  public readonly updated_at!: Date
}

Role.init({
  id: {
    allowNull: false,
    primaryKey: true,
    type: UUIDV4,
    defaultValue: DataTypes.UUIDV4,
  },
  role_name: {
    allowNull: false,
    type: DataTypes.STRING,
  }
}, {
  tableName: 'roles',
  timestamps: true,
  sequelize: sequelizeConnection,
  underscored: true,
})

// Role.belongsToMany(User, { through: UserRole, foreignKey: 'role_id' })

export default Role