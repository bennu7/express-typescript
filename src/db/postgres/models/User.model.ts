import { DataTypes, Model, CreationOptional, Association } from "sequelize";
import sequelizeConnection from "../../../config/postgres/dbConnect";
import Role from "./Role.model";
import UserRole from "./User_Role.model";


class User extends Model {
    public id!: CreationOptional<string>;
    public email!: string;
    public password!: string;
    public full_name!: string;
    public user_name!: string;
    public gender?: "LAKI-LAKI" | "PEREMPUAN";
    public phone_number?: string;

    public user_role?: UserRole;

    public readonly created_at!: Date
    public readonly updated_at!: Date
    public readonly deleted_at!: Date
}

User.init(
    {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        full_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        user_name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        gender: {
            type: DataTypes.STRING,
            allowNull: false
        },
        phone_number: {
            type: DataTypes.STRING,
            allowNull: true
        },
        created_at: {
            type: DataTypes.TIME,
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.TIME,
            defaultValue: DataTypes.NOW
        },
        deleted_at: {
            type: DataTypes.TIME,
        },
    },
    {
        tableName: "users",
        sequelize: sequelizeConnection,
        // freezeTableName: true,
        timestamps: false,
    },
);

User.hasOne(Role, { foreignKey: "user_id", as: "user_role" })
User.belongsToMany(Role, { through: UserRole, foreignKey: "user_id" })
Role.belongsToMany(User, { through: UserRole, foreignKey: 'role_id' })
UserRole.belongsTo(Role, { foreignKey: "role_id", as: "role" })


export default User;
