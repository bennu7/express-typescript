import { Schema, Types, model } from "mongoose"
import { ICompany, ILocation } from "../../../utils/types"
import { timeStamp } from "console"

const locationSchema = new Schema<ILocation>({
    city: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: false
    }
})

const companySchema = new Schema<ICompany>({
    name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    companySize: {
        type: Number,
        required: true
    },
    about: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    website: {
        type: String,
        required: true
    },
    userID: {
        type: Types.ObjectId,
        required: true,
        ref: "users",
    }
}, { timestamps: true })

export const companies = model<ICompany>("companies", companySchema)