import { fork } from "child_process"
import { } from "cluster"
import { Response, Request, NextFunction, ErrorRequestHandler } from 'express';

export class AppError extends Error {
    // *super constructor from this Error class have variable name, message, and stack
    code: number;

    constructor(code: number, message: string) {
        super(message);
        this.code = code;
        this.stack = process.env.NODE_ENV === 'production' ? '' : this.stack;
    }
}


const errorHandler: ErrorRequestHandler = (error: any, req: Request, res: Response, next: NextFunction) => {
    console.log("error => ", error);
    console.log("error.message => ", error.message);
    console.log("code => ", error.code);
    console.log("code => ", typeof error.code);
    // if (error == error instanceof MongooseError) {
    //     error = error instanceof MongooseError
    // }

    // const code: number = error.code || 500;
    const code: number = error.code || 500;
    const message: string = error.message || "internal Server Error";
    const name: string = error.name || "internal Server Error";

    if (error.code === "42703") {
        return res.status(code).json({
            statusCode: error.code,
            success: false,
            message: error.message,
        })
    }


    if (!(error instanceof AppError)) {
        // *custom message error
        console.log("error not instance AppError: ", error);
        return res.status(code).json({
            statusCode: code,
            success: false,
            // message: "The problem is on our end. We are working on fixing it.",
            message,
            data: null
        })
    }

    if (error instanceof AppError) {
        // *custom message error
        console.log("error not instance AppError: ", error);
        return res.status(code).json({
            statusCode: code,
            success: false,
            // message: "The problem is on our end. We are working on fixing it.",
            message,
            data: null
        })
    }
    return res.status(code).json({
        statusCode: code,
        success: false,
        message,
        data: null
    })


    // const statusCode: number = 500
    // const message: string = error.message || "Internal Server Error";

    // res.status(statusCode).json({
    //     statusCode,
    //     success: false,
    //     message,
    //     data: null
    // });
    // return;
};

export default errorHandler;