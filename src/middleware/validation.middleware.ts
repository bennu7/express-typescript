import { ClassConstructor, plainToClass } from "class-transformer"
import { ValidationError, validate } from "class-validator"
import { RequestHandler } from "express";
import { StatusCodes as status } from "http-status-codes";

function getConstraintValue(obj: object): unknown {
    for (const prop in obj) {
        if (prop === "constraints") {
            return obj[prop];
        } else if (typeof obj[prop] === "object") {
            const result = getConstraintValue(obj[prop]);
            if (result) {
                return result;
            }
        }
    }
}

const ValidationMiddleware = (
    type: ClassConstructor<object>,
    value: string | "body" | "query" | "params" = "body",
    skipMissingProperties = false,
    whitelist = true,
    forbidNonWhitelisted = true,
): RequestHandler => {
    return (req, res, next) => {
        validate(plainToClass(type, req[value]), {
            skipMissingProperties,
            whitelist,
            forbidNonWhitelisted,
        }).then((errors: ValidationError[]) => {
            if (errors.length > 0) {
                const message = errors.map((error: ValidationError) =>
                    Object.values(getConstraintValue(error) as unknown as string),
                );
                return res
                    .status(status.UNPROCESSABLE_ENTITY)
                    .json(
                        {
                            code: status.UNPROCESSABLE_ENTITY,
                            status: "UNPROCESSABLE ENTITY",
                            message: message[0][0]
                        }
                    );
            } else {
                next();
            }
        });
    };
};

// const ValidationMiddleware (
//     type: ClassConstructor<object>,
//     value: string | "body" | "query" | "params" = "body",
//     skipMissingProperties = false,
//     whiteList = true,
//     forbidNonWhiteListed = true,
// ): RequestHandler => {
//     return (req, res, next) => {
//         validate(plainToClass(type, req[value]), {
//             skipMissingProperties,
//             whiteList,
//             forbidNonWhiteListed,
//         }).then((errors: ValidationError[]) => {

//         })
//     }
// }

export default ValidationMiddleware;