export interface UserInterface {
    id: string;
    email: string;
    password: string;
    full_name: string;
    user_name: string;
    phone_number?: string;
    gender?: string;
    created_at?: Date;
    updated_at?: Date;
    role?: string;
    avatar?: string;
}