export interface APIResponseInterface {
    code: number,
    status: string,
    message: string,
    data?: any,
    errors?: any | object
}